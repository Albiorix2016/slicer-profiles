# Slicer Configurations

This is an attempt to collect some of the slicer settings I am using, as well as generating a
config for Orca Slicer, heavily inspired by Ellis's Super Slicer configs, and Mak's
Prusa configs.

This is currently experimental and Orca Slicer is not yet stable and needs to be tested more.
